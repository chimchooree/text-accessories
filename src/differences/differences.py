## Dump text in 1 + other text in 2 and I'll highlight the differences, line by line

def print_diff(diff):
    for item in diff:
        print(item)

def compare_list(one, two):
    return one

# Convert list.txt to list
def make_list(list_file):
    lines = list()
    with open(list_file, "r") as f:
        lines = f.read().splitlines()
    return lines

# Flow of Program
def main():
    file_one = '1.txt' # File containing Text 1
    file_two = '2.txt' # File containing Text 2
    lines_one = make_list(file_one)
    lines_two = make_list(file_two)
    print_diff(compare_list(lines_one, lines_two))

## Start Program ##

main()
